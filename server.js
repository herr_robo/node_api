const express = require("express");
const cors = require("cors");
const PORT = process.env.PORT || 5000;
const app = express();

var corsOptions = {
  origin: "http://localhost:3000"
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({extended: true}));

/*
// Commented out for now with recent updates in place
app.use(function (req, res, next) {

  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);

  next();
});
*/

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to anime application." });
});

require("./app/routes/anime.routes.js")(app);
require("./app/routes/user.routes.js")(app);

// listen for requests
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});