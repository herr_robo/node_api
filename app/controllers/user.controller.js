const bcrypt = require("bcryptjs/dist/bcrypt");
const User = require("../models/user.model.js");

// Register
exports.register = async (req, res, next) => {
    const { username, password } = req.body;

    if (password.length < 6) {
        return res.status(400)
            .json({ message: "Password less than 6 characters"});
    }

    try {
        await User.create({
            username,
            password,
        }).then(user =>{
            res.status(200).json({
                message: "User successfully created",
                user,
            })
        })
    } catch (error) {
        res.status(401)
            .json({
                message: "User not successfully created",
                error: error.message,
            });
    }
}


// Login to Site WIP
exports.login = async (req, res) => {

    const { username, password } = req.body;

    if (!username || !password) {
        return res.status(400).json({
            message: "Username or Password not provided",
        })
    }

    try {
        const user = await User.login(username);
        if (!user) {
            res.status(400).json({
                message: "Login not successful",
                error: "User not found"
            });
        } else {

            bcrypt.compare(password, user.password)
                .then(result => {
                    result 
                        ? 
                            res.status(200).json({
                                message: "Login successful",
                                user: {
                                    id: user.id,
                                    username: user.username,
                                    created_at: user.created_at
                                }
                            })
                        :   res.status(400).json({
                                message: "Login not successful"
                            })
                })
        }
    }
    catch (error) {
        res.status(400).json({
            message: "An error occurred",
            error: error.message
        })
    }
}
