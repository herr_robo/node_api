const Anime = require("../models/anime.model.js");

// Create and Save a new Anime
exports.create = async (req, res) => {
    const anime = req.body;

    if ((!anime.title) || (!anime.mal_id)) {
        return res.status(400)
            .json({ message: "Title or Mal Id is empty"});
    }

    try {
        await Anime.create(anime)
            .then(data => {
                res.status(200).json({
                    message: "Anime record successfully created",
                    data,
                })
            })
    }
    catch (error) {
        console.log('catch error: ', error);
        res.status(401)
            .json({
                message: "Anime not successfully created",
                error: error.message,
            });
    }
};

// Retrieve all Animes from the database (with condition later)
exports.findAll = async (req, res) => {
    const title = req.query.title;

    try {
        await Anime.getAll({title})
            .then(data => {
                res.status(200).json({
                    message: "Anime records successfully retrieved",
                    data,
                })
            })
    } catch (error) {
        console.log('catch error: ', error);
        res.status(401)
            .json({
                message: "Error attempting to retrieve Anime records",
                error: error.message
            });
    }
};

// Find a single Anime with an id
exports.findOne = async (req, res) => {
    const id = req.params.id;

    if (!id) {
        res.status(400).send({
            message: 'Id can not be empty!'
        });
    }

    try {
        await Anime.findById(id)
        .then(data => {
            res.status(200)
                .json({
                    message: "Anime record results",
                    results: data
                })
        })
    }
    catch (error) {
        console.log('catch error: ', error);
        res.status(401)
            .json({
                message: "Error attempting to retrieve Anime record",
                error: error.message
            })
    }
};

// Update an Anime identified by the id in the request
exports.updateWatchStatus = async (req, res) => {
    const anime = req.body;

    if ((!anime.anime_id) || (!anime.watch_status) || (!anime.episodes_watched) || ((typeof anime.watch_status) !== 'string') || (!Number.isInteger(parseInt(anime.episodes_watched)))) {
        return res.status(400).json({
            message: 'Bad Request: malformed data sent'
        });
    }

    try {
        await Anime.findById(anime.anime_id)
            .then(data => {
                if (data.length < 1) {
                    res.status(404).json({
                        message: "Anime record not availabe to update"
                    });
                }
                else if (anime.episodes_watched > data[0].episodes) {
                    return res.status(400).json({
                        message: 'Bad Request: Episodes Watched exceeds Number Episodes from Anime'
                    });
                }
                else {
                    Anime.updateWatchStatus(anime)
                        .then(data => {
                            res.status(200).json({
                                message: "Anime Watch Status and Episodes successfully updated",
                                id: data.anime_id,
                                watch_status: data.watch_status,
                                episodes_watched: data.episodes_watched
                            })
                        })
                }
            })
    } catch (error) {
        console.log('catch error: ', error);
        res.status(500)
            .json({
                message: "Anime Watched Status and Episodes Watched not updated",
                error: error.message
            });
    }
};

// Update an Anime identified by the id in the request for Favorite
exports.updateByIdFavorite = async (req, res) => {
    const anime = req.body;
    if ((!anime.anime_id) || (!Number.isInteger(anime.favorite))) {
        return res.status(400).json({
            message: 'Bad request'
        });
    }
    try {
        await Anime.findById(anime.anime_id)
            .then(data => {
                if (data.length < 1) {
                    res.status(404).json({
                        message: "Anime record not availabe to update"
                    });
                }
                else {
                    Anime.updateFavoriteStatus(anime)
                        .then(data => {
                            res.status(200).json({
                                message: "Anime Favorite successfully updated",
                                id: data.anime_id,
                                favorite: data.favorite
                            })
                        })
                }
            })
    } catch (error) {
        console.log('catch error: ', error);
        res.status(500)
            .json({
                message: "Anime Favorite not updated",
                error: error.message
            });
    }
};

// Delete single Anime from the database
exports.delete = async (req, res) => {
    const id = req.params.id;

    if (!id) {
        console.log('no id');
        res.status(400)
            .json({ message: "No Id provided"});
    }

    try {
        await Anime.findById(id)
            .then(data => {
                if (data.length < 1) {
                    res.status(404).json({
                        message: "Anime record not availabe to delete"
                    });
                }
                else  {
                    Anime.remove(id)
                    .then(data => {
                        res.status(200).json({
                            message: "Anime successfully deleted",
                            affectedRows: data.affectedRows,
                            anime_id: id
                    });
                })
                }
            })
    } catch (error) {
        console.log('catch error: ', error);
        res.status(500)
            .json({
                message: "Anime not successfully deleted",
                error: error.message
            });
    }
};