const pool = require("./connect.js");
const bcrypt = require('bcryptjs');

const User = function(user) {
    this.username = user.username;
    this.password = user.password;
    this.created_at = user.created_at;
}

User.create = async (newUser, res) => {
    try {
        let salt = bcrypt.genSaltSync(8);
        let hashpassword = bcrypt.hashSync(newUser.password, salt);

        let insert = "INSERT INTO users (username, password) values(?,?)" ;
        var data = [newUser.username, hashpassword];

        const conn = await pool.getConnection();
        const rows = await conn.query(insert, data);

        let select = "SELECT username, created_at FROM users WHERE id=?";
        let id = [rows[0].insertId];
        let returned = await conn.query(select, id)
        conn.release();

        return returned[0][0];
    }

    catch (error) {
        console.log('Error:', error);
        throw error;
    }
};

// Find based on username
User.login = async (username, res) => {

    try {
        let select = "SELECT * FROM users WHERE username=?"
        const conn = await pool.getConnection();
        const rows = await conn.query(select, username);
    
        conn.release();
        return rows[0][0];
    }
    catch (error) {
        console.log('Error:', error);
        throw error;
    }
}

module.exports = User;