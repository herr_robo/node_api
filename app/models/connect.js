const mysql = require('mysql2/promise');
const dbConfig = require('../config/db.config.js');

const pool = mysql.createPool({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB
});

/* // Keep for now, until sure it can be cut
connection.connect((err) => {
    if(err) {
        return console.error('error: ' + err.message);
    }

    console.log('Connected to the MySQL server.');
});
*/
module.exports = pool;