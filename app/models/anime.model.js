const pool = require("./connect.js");

const Anime = function(anime) {
    this.title = anime.title;
    this.mal_id = anime.mal_id;
    this.url = anime.url;
    this.image_url = anime.image_url;
    this.airing = anime.airing;
    this.synopsis = anime.synopsis;
    this.anime_type = anime.anime_type;
    this.episodes = anime.episodes;
    this.score = anime.score;
    this.start_date = anime.start_date;
    this.end_date = anime.end_date;
    this.rated = anime.rated;
    this.show_status = anime.show_status;
    this.watch_status = anime.watch_status;
    this.episodes_watched = anime.episodes_watched;
    this.duration = anime.duration;
    this.favorite = anime.favorite;
};

Anime.create = async (newAnime, result) => {
    try {
        let insert = `INSERT INTO anime 
            (
                title, mal_id, url, image_url, airing, synopsis, anime_type, episodes, score, start_date, 
                end_date, rated, show_status, watch_status, episodes_watched, duration, favorite
            )
            values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;

        let data = [
            newAnime.title, newAnime.mal_id, newAnime.url, newAnime.image_url, 
            newAnime.airing, newAnime.synopsis, newAnime.anime_type, newAnime.episodes, 
            newAnime.score, newAnime.start_date, newAnime.end_date, newAnime.rated, 
            newAnime.show_status, newAnime.watch_status, newAnime.episodes_watched, 
            newAnime.duration, newAnime.favorite
        ];

        const conn = await pool.getConnection();
        const rows = await conn.query(insert, data);

        let id = [rows[0].insertId];
        let select = "SELECT * FROM anime WHERE anime_id=?";
        let returned = await conn.query(select, id);

        conn.release();
        return  returned[0][0];

    }
    catch (error) {
        console.log("Error: ", error);
        throw error;
    }
};

Anime.findById = async (id, result) => {
    try {
        let find = "SELECT * FROM anime WHERE anime_id=?";
        const conn = await pool.getConnection();
        const rows = await conn.query(find, id);
        
        conn.release();
        return rows[0];
    }

    catch (error) {
        console.log('error:', error);
        throw error;
    }
};

Anime.getAll = async (title, result) => {
    try {
        let select = "SELECT * FROM anime";
        const conn = await pool.getConnection();
        const rows = await conn.query(select);
    
        conn.release();
        return rows[0];
    }
    catch (error) {
        console.log("Error: ", error);
        throw error;
    }
};

Anime.updateWatchStatus = async (anime, result) => {
    try {
        const update = `UPDATE anime SET watch_status=?, episodes_watched=? WHERE anime_id=?`;
        const data = [anime.watch_status, anime.episodes_watched, anime.anime_id];

        const conn = await pool.getConnection()
        const rows = await conn.query(update, data);
        conn.release();

        const returnedChange = {
            anime_id: anime.anime_id,
            watch_status: anime.watch_status,
            episodes_watched: anime.episodes_watched,
            changedRows: rows[0].changedRows,
            affectedRows: rows[0].affectedRows
        };

        return returnedChange;
    }
    catch (error) {
        console.log("Error: ", error);
        throw error;
    }
};

Anime.updateFavoriteStatus = async (favorite, result) => {
    try {
        const update = "UPDATE anime SET favorite=? WHERE anime_id=?";
        const data = [favorite.favorite, favorite.anime_id];
    
        const conn = await pool.getConnection();
        const rows = await conn.query(update, data);
        
        conn.release();
        const returnedChange = {
            anime_id: favorite.anime_id,
            favorite: favorite.favorite,
            changedRows: rows[0].changedRows,
            affectedRows: rows[0].affectedRows
        };
    
        return returnedChange;
    }
    catch (error) {
        console.log("Error: ", error);
        throw error;
    }
};

Anime.remove = async (id, result) => {
    try {
        let deleted = "DELETE FROM anime WHERE anime_id=?";
        const conn = await pool.getConnection();
        const rows = await conn.query(deleted, id);

        conn.release();
        return rows[0];

    }
    catch (error) {
        console.log("Error: ", error);
        throw error;
    }
};

// Anime.removeAll later

module.exports = Anime;