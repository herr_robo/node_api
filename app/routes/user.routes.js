module.exports = app => {
    const user = require("../controllers/user.controller.js");

    var router = require('express').Router();
    
    // register User
    router.post("/register", user.register);   
    
    // login with User
    router.post("/login", user.login);

    app.use('/api/user', router);
};