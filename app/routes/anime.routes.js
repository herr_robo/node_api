// /anime - GET, POST, DELETE
// /anime/:id - GET, PUT, DELETE

module.exports = app => {
    const anime = require("../controllers/anime.controller.js");

    var router = require('express').Router();

    // create a new Anime
    router.post("/", anime.create);

    // retrieve all anime
    router.get("/", anime.findAll);

    // retrieve a single Anime with id
    router.get("/:id", anime.findOne);

    // update an Anime Watch Status and Episodes
    router.put("/episodes", anime.updateWatchStatus);

    // update an Anime Favorite with Id
    router.put("/favorite", anime.updateByIdFavorite);

    // Delete an Anime with id
    router.delete("/:id", anime.delete);

    app.use('/api/anime', router);
};