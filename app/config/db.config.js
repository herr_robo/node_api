
// replace config properties with your own 
// to connect to your personal database
// a production environment would not expose these like below
// this is just a personal project for educational purposes

module.exports = {
    HOST: 'localhost',
    USER: 'root',
    PASSWORD: 'password',
    DB: 'interests',
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};